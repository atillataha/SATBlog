﻿using SatBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SatBlog.Controllers
{
    public class DetailsController : Controller
    {
        BlogSATimEntities db = new BlogSATimEntities();

        // GET: Details
        public ActionResult Index(int id)
        {
            var settings = db.BlogSettings.Where(x => x.id > 0).FirstOrDefault();
            ViewBag.Logo = settings.SITE_LOGO;
            ViewData["Mail"] = settings.USER_MAİL;
            TempData["atilla"] = "facebook.com";
            var deger = db.BlogText.Where(x => x.id == id).FirstOrDefault();
            ViewBag.Title = deger.BLOG_NAME;
            return View(deger);
        }
    }
}