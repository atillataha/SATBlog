﻿using SatBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SatBlog.Controllers
{
    public class CategoriController : Controller
    {
        BlogSATimEntities db = new BlogSATimEntities();

        // GET: Categori
        public ActionResult Index(int id)
        {
            return View(db.BlogText.Where(x=>x.BLOG_CATEGORİE_ID==id).ToList());
        }
    }
}