﻿using SatBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SatBlog.Controllers
{
    public class HomeController : Controller
    {
        BlogSATimEntities db = new BlogSATimEntities();

        // GET: Home
        public ActionResult Index()
        {
            var settings = db.BlogSettings.Where(x => x.id > 0).FirstOrDefault();
            ViewBag.Logo = settings.SITE_LOGO;
            ViewData["Mail"] = settings.USER_MAİL;
            TempData["atilla"] = "facebook.com";
     

            ViewBag.Title = "SATim Blog V1";


            return View(db.BlogText.ToList());
        }
        public ActionResult AboutMe()
        {

            return View(db.BlogSettings.FirstOrDefault());
        }
        public ActionResult PopulerPost()
        {

            return View(db.BlogText.OrderByDescending(x=>x.Comments.Count()).ToList());
        }
        public ActionResult Categories()
        {

            return View(db.Categories.ToList());
        }
        public ActionResult RecentPost()
        {

            return View(db.BlogText.OrderByDescending(x=>x.id).ToList());
        }
        public ActionResult TagsPost()
        {

            return View(db.Tags.ToList());
        }
        public ActionResult Archisves()
        {

            return View(db.BlogText.ToList());
        }
        public ActionResult Social()
        {

            return View(db.BlogSettings.FirstOrDefault());
        }
    }
}