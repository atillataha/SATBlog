﻿using SatBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SatBlog.Controllers
{
    public class BaseController : Controller
    {

        // GET: Base
        public ActionResult Index()
        {
            return View();
        }
    }
}