﻿using SatBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SatBlog.Controllers
{
    public class TagsController : Controller
    {
        BlogSATimEntities db = new BlogSATimEntities();

        // GET: Tags
        public ActionResult Index(int id)
        {
            return View(db.BlogText.Where(x=>x.id==id).ToList());
        }
    }
}